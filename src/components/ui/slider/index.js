// .js-slider-default

const defElSlider = document.querySelector('.js-slider-default')
if (defElSlider) {
  const defElSliderContainer = defElSlider.querySelector('.swiper-container')

  if (defElSliderContainer) {
    const defSlider = new Swiper(defElSliderContainer, {
      loop: true,
      spaceBetween: 16,

      navigation: {
        nextEl: defElSlider.querySelector('.slider__nav_next'),
        prevEl: defElSlider.querySelector('.slider__nav_prev'),
      }
    })
  }
}
