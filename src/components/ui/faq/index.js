class Faq {
  constructor(element, options) {
    if (!element) return
    this.$el = element
    this.$head = this.$el.querySelector('[data-faq-head]')
    this.$body = this.$el.querySelector('[data-faq-body]')

    this.options = options

    this.isShow = this.$el.dataset['faq'] == 'true' ? true : false

    this.setup()
  }

  setup() {
    this.$el.addEventListener('click', this.handleClick.bind(this))
    this.updateShow()
  }

  updateShow() {
    if (this.isShow) {
      this.$el.classList.add('is-active')
    } else {
      this.$el.classList.remove('is-active')
    }
  }

  setShow() {
    this.isShow = true
    this.updateShow()
  }

  setHide() {
    this.isShow = false
    this.updateShow()
  }

  handleClick(e) {
    this.isShow ? this.setHide() : this.setShow()
    if (this.options.onUpdate) this.options.onUpdate(this)
  }
}

function initAllFaq() {
  window.faqs = []
  Array.prototype.forEach.call(
    document.querySelectorAll('[data-faq]'),
    item => {
      window.faqs.push(new Faq(item, {
        onUpdate: (faq) => {
          window.faqs.forEach(f => {
            if (f !== faq) f.setHide()
          })
        }
      }))
    }
  )
}

initAllFaq()
