// data-tabs
// data-tabs-tab
// data-tabs-pane

class TabsItem {
  constructor(domElement, options) {
    this.options = {
      dataTabsCurrent: 'data-tabs-current',
      dataTab: 'data-tabs-tab',
      dataPane: 'data-tabs-pane',
      classActiveTab: 'tabs__tab_active',
      classActivePane: 'tabs__pane_active',
      ...options
    }

    this.$el = domElement
    this.tabs = {}
    this.panes = {}
    this.currentId = this.$el.getAttribute(`${this.options.dataTabsCurrent}`) || '0'

    this.setup()
  }

  setup() {
    this.setupTabs()
    this.setupPanes()
    this.changeVisibilityTab()
  }

  setupTabs() {
    const tabs = this.$el.querySelectorAll(`[${this.options.dataTab}]`)
    if (!tabs.length) return;
    tabs.forEach((tab, idx) => {
      const id = tab.getAttribute(this.options.dataTab)
      this.addHandlerClickTab(tab)
      this.tabs[id] = tab
    })
  }
  setupPanes() {
    const panes = this.$el.querySelectorAll(`[${this.options.dataPane}]`)
    if (!panes.length) return;
    panes.forEach(pane => {
      const id = pane.getAttribute(this.options.dataPane)
      this.panes[id] = pane
    })
  }

  addHandlerClickTab(tab) {
    tab.addEventListener('click', this.handlerClickTab.bind(this, tab), false)
  }

  handlerClickTab(tab, e) {
      this.currentId = tab.getAttribute(`${this.options.dataTab}`)
      this.changeVisibilityTab()
  }

  changeVisibilityTab() {
    Object.values(this.tabs).forEach(t => t.classList.remove(this.options.classActiveTab))
    Object.values(this.panes).forEach(p => p.classList.remove(this.options.classActivePane))

    this.tabs[this.currentId].classList.add(this.options.classActiveTab)
    this.panes[this.currentId].classList.add(this.options.classActivePane)
  }

  update() {}
  destroy() {}
}

Array.prototype.forEach.call(document.querySelectorAll('[data-tabs]'), tabs => {
  new TabsItem(tabs)
})
