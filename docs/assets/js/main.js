"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

// TABS
// data-tabs
// data-tabs-tab
// data-tabs-pane
var TabsItem = /*#__PURE__*/function () {
  function TabsItem(domElement, options) {
    _classCallCheck(this, TabsItem);

    this.options = _objectSpread({
      dataTabsCurrent: 'data-tabs-current',
      dataTab: 'data-tabs-tab',
      dataPane: 'data-tabs-pane',
      classActiveTab: 'tabs__tab_active',
      classActivePane: 'tabs__pane_active'
    }, options);
    this.$el = domElement;
    this.tabs = {};
    this.panes = {};
    this.currentId = this.$el.getAttribute("".concat(this.options.dataTabsCurrent)) || '0';
    this.setup();
  }

  _createClass(TabsItem, [{
    key: "setup",
    value: function setup() {
      this.setupTabs();
      this.setupPanes();
      this.changeVisibilityTab();
    }
  }, {
    key: "setupTabs",
    value: function setupTabs() {
      var _this = this;

      var tabs = this.$el.querySelectorAll("[".concat(this.options.dataTab, "]"));
      if (!tabs.length) return;
      tabs.forEach(function (tab, idx) {
        var id = tab.getAttribute(_this.options.dataTab);

        _this.addHandlerClickTab(tab);

        _this.tabs[id] = tab;
      });
    }
  }, {
    key: "setupPanes",
    value: function setupPanes() {
      var _this2 = this;

      var panes = this.$el.querySelectorAll("[".concat(this.options.dataPane, "]"));
      if (!panes.length) return;
      panes.forEach(function (pane) {
        var id = pane.getAttribute(_this2.options.dataPane);
        _this2.panes[id] = pane;
      });
    }
  }, {
    key: "addHandlerClickTab",
    value: function addHandlerClickTab(tab) {
      tab.addEventListener('click', this.handlerClickTab.bind(this, tab), false);
    }
  }, {
    key: "handlerClickTab",
    value: function handlerClickTab(tab, e) {
      this.currentId = tab.getAttribute("".concat(this.options.dataTab));
      this.changeVisibilityTab();
    }
  }, {
    key: "changeVisibilityTab",
    value: function changeVisibilityTab() {
      var _this3 = this;

      Object.values(this.tabs).forEach(function (t) {
        return t.classList.remove(_this3.options.classActiveTab);
      });
      Object.values(this.panes).forEach(function (p) {
        return p.classList.remove(_this3.options.classActivePane);
      });
      this.tabs[this.currentId].classList.add(this.options.classActiveTab);
      this.panes[this.currentId].classList.add(this.options.classActivePane);
    }
  }, {
    key: "update",
    value: function update() {}
  }, {
    key: "destroy",
    value: function destroy() {}
  }]);

  return TabsItem;
}();

Array.prototype.forEach.call(document.querySelectorAll('[data-tabs]'), function (tabs) {
  new TabsItem(tabs);
}); // SLIDER
// .js-slider-default

var defElSlider = document.querySelector('.js-slider-default');

if (defElSlider) {
  var defElSliderContainer = defElSlider.querySelector('.swiper-container');

  if (defElSliderContainer) {
    var defSlider = new Swiper(defElSliderContainer, {
      loop: true,
      spaceBetween: 16,
      navigation: {
        nextEl: defElSlider.querySelector('.slider__nav_next'),
        prevEl: defElSlider.querySelector('.slider__nav_prev')
      }
    });
  }
} // FAQ


var Faq = /*#__PURE__*/function () {
  function Faq(element, options) {
    _classCallCheck(this, Faq);

    if (!element) return;
    this.$el = element;
    this.$head = this.$el.querySelector('[data-faq-head]');
    this.$body = this.$el.querySelector('[data-faq-body]');
    this.options = options;
    this.isShow = this.$el.dataset['faq'] == 'true' ? true : false;
    this.setup();
  }

  _createClass(Faq, [{
    key: "setup",
    value: function setup() {
      this.$el.addEventListener('click', this.handleClick.bind(this));
      this.updateShow();
    }
  }, {
    key: "updateShow",
    value: function updateShow() {
      if (this.isShow) {
        this.$el.classList.add('is-active');
      } else {
        this.$el.classList.remove('is-active');
      }
    }
  }, {
    key: "setShow",
    value: function setShow() {
      this.isShow = true;
      this.updateShow();
    }
  }, {
    key: "setHide",
    value: function setHide() {
      this.isShow = false;
      this.updateShow();
    }
  }, {
    key: "handleClick",
    value: function handleClick(e) {
      this.isShow ? this.setHide() : this.setShow();
      if (this.options.onUpdate) this.options.onUpdate(this);
    }
  }]);

  return Faq;
}();

function initAllFaq() {
  window.faqs = [];
  Array.prototype.forEach.call(document.querySelectorAll('[data-faq]'), function (item) {
    window.faqs.push(new Faq(item, {
      onUpdate: function onUpdate(faq) {
        window.faqs.forEach(function (f) {
          if (f !== faq) f.setHide();
        });
      }
    }));
  });
}

initAllFaq();